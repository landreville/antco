# AntCo

An implementation of Ant Colony Optimization. 

This library can be used both as a Rust crate and as a Python package.

See [https://landreville.gitlab.io/antco](https://landreville.gitlab.io/antco/antco/) for documentation.

Example Rust usage:

```rust
use antco::{aco, AcoParameters};

fn main() {
    let destinations: Vec<i32> = vec![4, 2, 3, 5, 1, 6, 8, 7, 9, 0];
    let result = aco(
        &destinations,
        |a: &i32, b: &i32| -> f64 { (b - a).abs() as f64 },
        AcoParameters::default()
    );

    println!("{result:?}");
}
```

Example Python usage:
```python
from antco import aco, distance_matrix

destinations = [4, 2, 3, 5, 1, 6, 8, 7, 9, 0]
distances = distance_matrix(destinations, lambda a, b: abs(b-a))
result = aco(destinations, distances)
print(result)
```