from typing import List, TypeVar, Callable
from antco import _antco

T = TypeVar("T")


def aco(destinations: List[T], distances: List[List[float]]):
    order = _antco.run_aco(distances)
    return [destinations[i] for i in order]


def distance_matrix(
    items: List[T], distance_fn: Callable[[T, T], float]
) -> List[List[float]]:
    """Return a two-dimensional list of all combinations of items and their distance."""
    return [
        [distance_fn(i, j) for i in items] for j in items
    ]