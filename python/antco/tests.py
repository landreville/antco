import unittest
from itertools import tee

from antco import aco, distance_matrix


class TestAco(unittest.TestCase):

    def test_int_aco(self):
        destinations = [4, 2, 3, 5, 1, 6, 8, 7, 9, 0]
        distances = distance_matrix(destinations, lambda a, b: abs(b-a))

        result = aco(destinations, distances)

        in_order = len(list(filter(lambda pair: pair[0] <= pair[1], _pairwise(result))))
        # Must be all in order (either lowest to highest or highest to lowest)
        self.assertTrue(
            in_order == len(destinations) or in_order == 0,
            f"Found: {result}"
        )
        self.assertTrue(all(el in destinations for el in result))


def _pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


if __name__ == '__main__':
    unittest.main()
