//! An implementation of Ant Colony Optimization.
//!
//! The ACO algorithm is run by calling [`aco`] with your destinations, a distance function, and
//! the algorithm parameters. Any type of destination should work, because you must provide
//! the distance function that works for them.
//!
//! Distances between every combination of destination will be calculated up-front. This includes
//! both directions -- distances are allowed to be different based on the order of arguments to
//! the distance function.
//!
//! The results will be a Vector of references to the given destinations. Now they will be in the
//! optimal order (or at least a local optimum).
use rand::distributions::Distribution;
use rand::distributions::WeightedIndex;
use rand::Rng;
use pyo3::prelude::*;

#[pyfunction]
fn run_aco(
    distances: Vec<Vec<f64>>,
) -> Vec<usize> {
    let destinations = (0..distances.len()).collect();
    let result = aco(&destinations, &distances, AcoParameters::default());
    result.into_iter().copied().collect()
}

// antco Python module.
#[pymodule]
fn _antco(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(run_aco, m)?)?;
    Ok(())
}

/// These are parameters to the ACO algorithm.
/// Default parameters can be used by calling `AcoParameters::default()`.
pub struct AcoParameters {
    /// How much the existing pheromone trails affects the ants choices. Default is 1.
    pub alpha: i32,
    /// How much the distance between destinations affects the ants choices. Default is 5.
    pub beta: i32,
    /// How much pheromone to lay down. Default is 500.
    pub q: f64,
    /// How many ants to use. Default is 1024.
    /// Recommended ratio is 85% of destinations with minimum 1024 -- based on my own subjectivity.
    pub ant_count: usize,
    /// How fast to evaporate pheromone on each step. Default is 0.2.
    pub evaporation_rate: f64,
}

impl Default for AcoParameters {
    fn default() -> Self {
        Self {
            alpha: 1,
            beta: 5,
            q: 500.0,
            ant_count: 1024,
            evaporation_rate: 0.2,
        }
    }
}

/// Run the Ant-Colony Optimization algorithm and return optimal path through destinations.
///
/// * destinations -- A Vec of references to your own type. This is only used to provide
///     the output Vector of references.
/// * distances -- Matrix of distances between destinations. The indexes must match the indexes
///     of destinations. The helper function `distance_matrix` can create this for you given
///     the destinations and a distance function.
/// * params -- See [`AcoParameters`] for the available parameters or use `AcoParameters::default()`.
///
/// Returns -- A Vec of references to the given destinations ordered in the optimal path.
pub fn aco<'a, T>(
    destinations: &'a Vec<T>,
    distances: &'a Vec<Vec<f64>>,
    params: AcoParameters,
) -> Vec<&'a T> {
    let mut rng = rand::thread_rng();
    // The index of destinations is used in the calculations and the length
    // is used as the iteration range in many places.
    let dest_count = destinations.len();

    // Each element is a path an ant takes.
    // Initialize with a random beginning destination.
    let mut ant_paths: Vec<Vec<usize>> = (0..params.ant_count)
        .map(|_| vec![rng.gen_range(0..dest_count)])
        .collect();

    // Pheromone trails between destinations is initialized with 1.0 of pheromone.
    let mut trails: Vec<Vec<f64>> = (0..dest_count).map(|_| vec![1.0; dest_count]).collect();

    // March those ants
    for _ in 0..dest_count - 1 {
        move_ants(
            &mut ant_paths,
            &distances,
            &trails,
            dest_count,
            params.alpha,
            params.beta,
        );
        evaporate(&mut trails, params.evaporation_rate);
        scent_trails(&mut trails, params.q, &ant_paths, &distances);
    }

    let mut optimal_trail: Vec<usize> = Vec::new();
    let mut min_dist: f64 = 0.0;
    for ant_path in ant_paths {
        let dist = path_distance(&ant_path, &distances);
        if dist < min_dist || min_dist == 0.0 {
            min_dist = dist;
            optimal_trail = ant_path;
        }
    }

    // We've been using indexes as a proxy for destination, now return back the
    // real destination references.
    optimal_trail.iter().map(|i| &destinations[*i]).collect()
}

pub fn distance_matrix<'a, T, F>(destinations: &'a Vec<T>, distance_fn: F) -> Vec<Vec<f64>>
where
    F: Fn(&'a T, &'a T) -> f64,
{
    // Currently allows for distance_fn(a, b) to have a
    // different value than distance_fn(b, a).
    destinations
        .iter()
        .enumerate()
        .map(|(_, dest_a)| {
            destinations
                .iter()
                .enumerate()
                .map(|(_, dest_b)| distance_fn(dest_a, dest_b))
                .collect::<Vec<f64>>()
        })
        .collect()
}

fn move_ants(
    ant_paths: &mut Vec<Vec<usize>>,
    distances: &Vec<Vec<f64>>,
    trails: &Vec<Vec<f64>>,
    dest_count: usize,
    alpha: i32,
    beta: i32,
) {
    for ant_path in ant_paths.iter_mut() {
        ant_path.push(find_next_destination(
            &ant_path, distances, trails, dest_count, alpha, beta,
        ));
    }
}

fn find_next_destination(
    ant_path: &Vec<usize>,
    distances: &Vec<Vec<f64>>,
    trails: &Vec<Vec<f64>>,
    dest_count: usize,
    alpha: i32,
    beta: i32,
) -> usize {
    let mut rng = rand::thread_rng();

    // Occasionally choose a random destination.
    if rng.gen_range(0..100) == 0 {
        let unvisited: Vec<usize> = (0..dest_count)
            .filter(|dest| !ant_path.contains(dest))
            .collect();
        unvisited[rng.gen_range(0..unvisited.len())]
    } else {
        let weights =
            destination_probabilities(&ant_path, &distances, &trails, dest_count, alpha, beta);

        if let Ok(wi) = WeightedIndex::new(&weights) {
            wi.sample(&mut rng)
        } else {
            rng.gen_range(0..dest_count)
        }
    }
}

fn destination_probabilities(
    ant_path: &Vec<usize>,
    distances: &Vec<Vec<f64>>,
    trails: &Vec<Vec<f64>>,
    dest_count: usize,
    alpha: i32,
    beta: i32,
) -> Vec<f64> {
    let mut pheromone_potential = 0.0;
    let current_city = ant_path[ant_path.len() - 1];
    let current_trails = &trails[current_city];
    let current_distances = &distances[current_city];

    // Calculate all the potential pheromone that could be laid down going to every destination
    for dest in 0..dest_count {
        if ant_path.contains(&dest) {
            // This ant has already visited this destination
            continue;
        }
        pheromone_potential +=
            current_trails[dest].powi(alpha) * ((1.0 / current_distances[dest]).powi(beta));
    }

    if pheromone_potential == 0.0 {
        // If there's no pheromone to be had, then every unvisited city has equal weight
        (0..dest_count)
            .map(|dest| if ant_path.contains(&dest) { 0.0 } else { 1.0 })
            .collect()
    } else {
        (0..dest_count)
            .map(|dest| {
                if ant_path.contains(&dest) {
                    // This ant already visited this destination
                    0.0
                } else {
                    // What's the potential pheromone strength of going to this destination
                    // as ratio to the total potential pheromone
                    (current_trails[dest].powi(alpha)
                        * ((1.0 / current_distances[dest]).powi(beta)))
                        / pheromone_potential
                }
            })
            .collect()
    }
}

fn evaporate(trails: &mut Vec<Vec<f64>>, evaporation_rate: f64) {
    for dest_a in trails {
        for dest_b in dest_a.iter_mut() {
            *dest_b *= evaporation_rate;
        }
    }
}

fn scent_trails(
    trails: &mut Vec<Vec<f64>>,
    q: f64,
    ant_paths: &Vec<Vec<usize>>,
    distances: &Vec<Vec<f64>>,
) {
    // Lay down some more pheromone on the trails
    for ant_path in ant_paths {
        let contribution = q / path_distance(ant_path, distances);
        ant_path
            .windows(2)
            .for_each(|w| trails[w[0]][w[1]] += contribution);
    }
}

fn path_distance(ant_path: &Vec<usize>, distances: &Vec<Vec<f64>>) -> f64 {
    ant_path
        .windows(2)
        .fold(0.0, |acc, w| acc + distances[w[0]][w[1]])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_int_destinations() {
        let destinations: Vec<i32> = vec![4, 2, 3, 5, 1, 6, 8, 7, 9, 0];
        let distances = distance_matrix(&destinations, |a: &i32, b: &i32| -> f64 {
            (b - a).abs() as f64
        });

        let result = aco(&destinations, &distances, AcoParameters::default());

        assert!(
            result.windows(2).all(|w| w[0] <= w[1]) || result.windows(2).all(|w| w[0] >= w[1]),
            "Result was not in order. This can also happen if the algorithm hit a local optimum."
        );

        destinations.iter().for_each(|val| {
            assert!(
                result.contains(&val),
                "Missing expected destination value: {val:?}. Found: {result:?}"
            )
        });
    }

    #[test]
    fn test_box_destinations() {
        let destinations = vec![
            Box::new(3),
            Box::new(1),
            Box::new(2),
            Box::new(4),
            Box::new(5),
        ];
        let distances = distance_matrix(&destinations, |a: &Box<isize>, b: &Box<isize>| -> f64 {
            (**b - **a).abs() as f64
        });

        let result = aco(&destinations, &distances, AcoParameters::default());

        assert!(
            result.windows(2).all(|w| w[0] <= w[1]) || result.windows(2).all(|w| w[0] >= w[1]),
            "Result was not in order. This can also happen if the algorithm hit a local optimum."
        );

        destinations.iter().for_each(|val| {
            assert!(
                result.contains(&val),
                "Missing expected destination value: {val:?}. Found: {result:?}"
            )
        });
    }
}
